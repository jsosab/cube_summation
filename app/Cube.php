<?php 
//include('/config/constants.php');

namespace App;
use App\Exceptions;
 
/**
* 
*/
class Cube
{
	


	const MIN_T = 1;
	const MAX_T = 50;
	const MIN_N = 1;
	const MAX_N = 100;
	const MIN_M = 1;
	const MAX_M = 1000;
	const MIN_W = -1000000000;
	const MAX_W = 100000000;

	public $cube;
	public $size_cube;

	function __construct($size, $cube)
	{
		# code...
        $this->size_cube = $size;
        $this->cube = $cube;
	}

	function Inicialize_Cube(){
		for ($a=0; $a < $this->size_cube; $a++) {
			for ($b=0; $b < $this->size_cube; $b++) {
				for ($c=0; $c < $this->size_cube; $c++) { 
					$cube[$a][$b][$c] = 0;
				}
			}
		}

		return $cube;
	}

	public function Validate_Range($min, $max, $value){

		if(($value < $min) || ($value > $max))
			return "Valores Fuera de Rango";

		return true;
			
	}

	public function update($x, $y, $z, $value) {

		$this->Validate_Range(MIN_W,MAX_W,$value);
		$this->Validate_Range(1,$this->size_cube,$x);
		$this->Validate_Range(1,$this->size_cube,$y);
		$this->Validate_Range(1,$this->size_cube,$z);
		$this->cube[$x][$y][$z] = $value;
		//return $this->cube[$x][$y][$z];
	}

    public function query($x1, $x2, $y1, $y2, $z1, $z2){

		$this->Validate_Range(1,$x2,$x1);
		$this->Validate_Range(1,$this->size_cube,$x2);
		$this->Validate_Range(1,$y2,$y1);
		$this->Validate_Range(1,$this->size_cube,$y2);
		$this->Validate_Range(1,$z2,$z1);
		$this->Validate_Range(1,$this->size_cube,$z2);

		$sum = 0;

		for ($a=$x1 ; $a<=$x2; $a++) { 
			for ($b=$y1; $b<=$y2; $b++) { 
				for ($c=$z1; $c<=$z2; $c++) { 
					$sum = $sum + $this->cube[$a][$b][$c];
				}
			}
		}

		return $sum;
	}

}



 ?>

