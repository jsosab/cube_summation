<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cube;

class CubeController extends Controller
{
    //

    public function MostrarSuma(){

        return view('suma');
    }

    public function Actualizar(Request $request){
        
        $Cube = new Cube(session('size'),session('cube'));
        $size = session('size');


        if($request->operation_type == 'UPDATE')
        {
            $Cube->update($request->coord_x,$request->coord_y,$request->coord_z,$request->value);
            $request->session()->flush();
            session(['size' => $size]);
            session(['cube' => $Cube->cube]);

            return response()->json("Cubo Actualizado!!!");

        }
        else
        {
            $suma = $Cube->query($request->coord_X1,$request->coord_X2,$request->coord_Y1,$request->coord_Y2,$request->coord_Z1,$request->coord_Z2);
            $request->session()->flush();
            session(['size' => $size]);
            session(['cube' => $Cube->cube]);

            return $suma;
        }


        
    }

    public function Iniciar(Request $request){
        $Cube = new Cube($request->size_cube,0);
        $cube_ini = $Cube->Inicialize_Cube();
        $Cube->cube = $cube_ini;
        session(['size' => $request->size_cube]);
        session(['cube' => $Cube->cube]);

        return response()->json("Cubo Inicializado!!!");
    }

    public function MostrarCubo(){
        
        $Cube = new Cube(session('size'),session('cube'));
        dd($Cube);

    }
}
