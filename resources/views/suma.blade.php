<!DOCTYPE html>
<html>
<head>
	<title>Suma de Cubo</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.css') }}">
	<script src="{{ asset('jquery/js/jquery-2.1.4.js') }}"></script>

</head>
<style type="text/css">

	.div_operations{
		width:200px;
	}

	.input_operations{
		width: 70px;
	}

</style>
<body>
<div class="container">
	<h3>Suma de Cubos</h3>
	<hr>
	<form action="/procesar" method="GET" id="form_cubo">
		<div class="form-group col-md-2">
			<label>Numero de Tests: </label>
			<input type="text" name="number_tests" id="number_tests" class="form-control">
			<input type="hidden" name="number_tests_hidden" id="number_tests_hidden">
		</div>
		<div class="form-group col-md-1">
			<button id="btn_test" name="btn_test" class="btn btn-primary" style="margin-top: 25px;" title="Agregar Tests" type="button">...</button>
		</div>
		<div id="test_group" class="col-md-12" style="display: none">
			<h4><b>Prueba <label id="test" name="test" value=""></label> - <label id="total_test" name="total_test" value=""></label></b></h4>
			<br>
			<div class='form-group col-md-6 div_operations'>
				<label>Tamaño del Cubo: </label>
					<input type='text' name='size_cube' id='size_cube' class='form-control input_operations'>
			</div>
			<div class='form-group col-md-6 div_operations'>
				<label>Numero de Operaciones: </label>
				<input type='text' name='operation' id='operation' class='form-control input_operations'>
				<input type="hidden" name="operation_hidden" id="operation_hidden">
			</div>
			<div class='form-group col-md-1'>
				<button id="btn_op" name='btn_op' class='btn btn-primary' style='margin-top: 25px;' title='Agregar Operaciones' type='button'>...</button>
			</div>
		</div>
		<br>
		<div id="operations_group" class="col-md-12" style="display: none">
			<h4>Operaciones Restantes: <label id="operations" name="operations" value=""></label></h4>
			<table class="table table-bordered table-stripped">
				<thead>
					<th>Tipo de Operacion</th>
					<th>X</th>
					<th>Y</th>
					<th>Z</th>
					<th>Valor</th>
					<th></th>
				</thead>
				<tbody>
					<td>
						<input type="text" name="operation_type_up" id="operation_type_up" class="form-control" value="UPDATE">
					</td>
					<td>
						<input type="text" name="coord_X" id="coord_X" class="form-control" value="">
					</td>
					<td>
						<input type="text" name="coord_Y" id="coord_Y" class="form-control" value="">
					</td>
					<td>
						<input type="text" name="coord_Z" id="coord_Z" class="form-control" value="">
					</td>
					<td>
						<input type="text" name="value" id="value" class="form-control" value="">
					</td>
					<td>
						<button id="btn_procesar_operacion_up" name='btn_procesar_operacion_up' class='btn btn-primary' title='Procesar Operacion' type='button'>...</button>
					</td>
				</tbody>
			</table>
			<hr>
			<table class="table table-bordered table-stripped">
				<thead>
					<th>Tipo de Operacion</th>
					<th>X1</th>
					<th>X2</th>
					<th>Y1</th>
					<th>Y2</th>
					<th>Z1</th>
					<th>Z2</th>
					<th></th>
				</thead>
				<tbody>
					<td>
						<input type="text" name="operation_type_qu" id="operation_type_qu" class="form-control" value="QUERY">
					</td>
					<td>
						<input type="text" name="coord_X1" id="coord_X1" class="form-control" value="">
					</td>
					<td>
						<input type="text" name="coord_X2" id="coord_X2" class="form-control" value="">
					</td>
					<td>
						<input type="text" name="coord_Y1" id="coord_Y1" class="form-control" value="">
					</td>
					<td>
						<input type="text" name="coord_Y2" id="coord_Y2" class="form-control" value="">
					</td>
					<td>
						<input type="text" name="coord_Z1" id="coord_Z1" class="form-control" value="">
					</td>
					<td>
						<input type="text" name="coord_Z2" id="coord_Z2" class="form-control" value="">
					</td>
					<td>
						<button id="btn_procesar_operacion_qu" name='btn_procesar_operacion_qu' class='btn btn-primary' title='Procesar Operacion' type='button'>...</button>
					</td>
				</tbody>
			</table>
			<button id="btn_clear" name='btn_clear' class='btn btn-primary' title='' type='button'>Limpiar</button>
			<br>
		</div>
		{{-- <div class="form-group">
			<button type="submit" class="btn btn-primary">Procesar</button>
		</div> --}}
	</form>
</div>
</body>
</html>
<script type="text/javascript">
	
	var cont_test = 1;
	var cont_operations = 0;
	var securitytoken = $('meta[name=csrf-token]').attr('content');
	var operations;
	var tests;

	$(document).ready(function(){
		$("#btn_test").click(function(){
			tests = $("#number_tests").val();
			$("#test").html(1);
			$("#test").val(1);
			$("#test_group").show();
			$("#total_test").html(tests);
			$("#size_cube").removeAttr('disabled', 'disabled');
			$("#operation").removeAttr('disabled', 'disabled');
			$("#btn_op").removeAttr('disabled', 'disabled');	
		});

		$("#btn_op").click(function(){

			if(($("#size_cube").val() == "") || ($("#operation").val()== "")){
				alert("Hay Entradas vacias!!!");
			}
			else{
				if(cont_test <= tests){
					operations = $("#operation").val();
					var securitytoken = $('meta[name=csrf-token]').attr('content');
					$.ajax({
						type: 'POST',
						url: '/Iniciar',
						data:{size_cube: $("#size_cube").val(), _token: securitytoken},
						success: function(data){
							$("#operations_group").show();
							$("#operations").html(operations);
							cont_operations = operations;
							$("#operation_type_up").removeAttr('disabled', 'disabled');
							$("#coord_X").removeAttr('disabled', 'disabled');
							$("#coord_Y").removeAttr('disabled', 'disabled');
							$("#coord_Z").removeAttr('disabled', 'disabled');
							$("#value").removeAttr('disabled', 'disabled');
							$("#btn_procesar_operacion_up").removeAttr('disabled', 'disabled');
							$("#operation_type_qu").removeAttr('disabled', 'disabled');
							$("#coord_X1").removeAttr('disabled', 'disabled');
							$("#coord_X2").removeAttr('disabled', 'disabled');
							$("#coord_Y1").removeAttr('disabled', 'disabled');
							$("#coord_Y2").removeAttr('disabled', 'disabled');
							$("#coord_Z1").removeAttr('disabled', 'disabled');
							$("#coord_Z2").removeAttr('disabled', 'disabled');
							$("#btn_procesar_operacion_qu").removeAttr('disabled', 'disabled');
							alert(data);
						},
						error: function(){

						}
					});	
				}
			}

		});

		$("#btn_clear").click(function(){
			$("#test_group").hide();
			$("#operations_group").hide();
			$("#number_tests").val("");
			$("#number_tests").removeAttr('disabled', 'disabled');
			$("#btn_test").removeAttr('disabled', 'disabled');
		});

		$("#btn_procesar_operacion_up").click(function(){
			//var securitytoken = $('meta[name=csrf-token]').attr('content');
			if(cont_operations > 0){
				if(($("#coord_X").val()=="") || ($("#coord_Y").val()=="") || ($("#coord_Z").val()=="") || ($("#value").val()==""))
				{
					alert("No se pueden dejar casillas vacias!!!");
				}
				else
				{
					$.ajax({
						type: 'POST',
						url: '/Procesar',
						data:{
							operation_type:$("#operation_type_up").val(),
							coord_x:$("#coord_X").val(),
							coord_y:$("#coord_Y").val(),
							coord_z:$("#coord_Z").val(), 
							value:$("#value").val(),
							_token: securitytoken
						},
						success: function(data){
							alert(data);
							cont_operations --;
							$("#operations").html(cont_operations);
							$("#coord_X").val('');
							$("#coord_Y").val('');
							$("#coord_Z").val(''); 
							$("#value").val('');
							if(cont_operations == 0){
								$("#operation_type_up").attr('disabled', 'disabled');
								$("#coord_X").attr('disabled', 'disabled');
								$("#coord_Y").attr('disabled', 'disabled');
								$("#coord_Z").attr('disabled', 'disabled');
								$("#value").attr('disabled', 'disabled');
								$("#btn_procesar_operacion_up").attr('disabled', 'disabled');
								$("#operation_type_qu").attr('disabled', 'disabled');
								$("#coord_X1").attr('disabled', 'disabled');
								$("#coord_X2").attr('disabled', 'disabled');
								$("#coord_Y1").attr('disabled', 'disabled');
								$("#coord_Y2").attr('disabled', 'disabled');
								$("#coord_Z1").attr('disabled', 'disabled');
								$("#coord_Z2").attr('disabled', 'disabled');
								$("#btn_procesar_operacion_qu").attr('disabled', 'disabled');
								$("#size_cube").val('');
								$("#operation").val('');
								if(cont_test < tests){
									cont_test ++;
									$("#test").html(cont_test);
									$("#test").val(cont_test);
								}
								else{

									$("#size_cube").attr('disabled', 'disabled');
									$("#operation").attr('disabled', 'disabled');
									$("#btn_op").attr('disabled', 'disabled');
									$("#number_tests").val('');
									$("#number_tests").attr('disabled', 'disabled');
									$("#btn_test").attr('disabled', 'disabled');
								}
							}
						},
						error: function(){

						}
					});
				}
				
			}
		});

		$("#btn_procesar_operacion_qu").click(function(){
			//var securitytoken = $('meta[name=csrf-token]').attr('content');
			if(cont_operations > 0){
				if(($("#coord_X1").val()=="") || ($("#coord_X2").val()=="") || ($("#coord_Y1").val()=="") || ($("#coord_Y2").val()=="")|| ($("#coord_Z1").val()=="") || ($("#coord_Z2").val()==""))
				{
					alert("No se pueden dejar casillas vacias!!!");
				}
				else
				{
					$.ajax({
						type: 'POST',
						url: '/Procesar',
						data:{
							operation_type:$("#operation_type_qu").val(),
							coord_X1:$("#coord_X1").val(),
							coord_X2:$("#coord_X2").val(),
							coord_Y1:$("#coord_Y1").val(),
							coord_Y2:$("#coord_Y2").val(),
							coord_Z1:$("#coord_Z1").val(),
							coord_Z2:$("#coord_Z2").val(),
							_token: securitytoken
						},
						success: function(data){
							alert(data);
							cont_operations --;
							$("#operations").html(cont_operations);
							$("#coord_X1").val('');
							$("#coord_X2").val('');
							$("#coord_Y1").val('');
							$("#coord_Y2").val('');
							$("#coord_Z1").val('');
							$("#coord_Z2").val('');
							if(cont_operations == 0){
								$("#operation_type_up").attr('disabled', 'disabled');
								$("#coord_X").attr('disabled', 'disabled');
								$("#coord_Y").attr('disabled', 'disabled');
								$("#coord_Z").attr('disabled', 'disabled');
								$("#value").attr('disabled', 'disabled');
								$("#btn_procesar_operacion_up").attr('disabled', 'disabled');
								$("#operation_type_qu").attr('disabled', 'disabled');
								$("#coord_X1").attr('disabled', 'disabled');
								$("#coord_X2").attr('disabled', 'disabled');
								$("#coord_Y1").attr('disabled', 'disabled');
								$("#coord_Y2").attr('disabled', 'disabled');
								$("#coord_Z1").attr('disabled', 'disabled');
								$("#coord_Z2").attr('disabled', 'disabled');
								$("#btn_procesar_operacion_qu").attr('disabled', 'disabled');
								$("#size_cube").val('');
								$("#operation").val('');
								if(cont_test < tests){
									cont_test ++;
									$("#test").html(cont_test);
									$("#test").val(cont_test);
								}
								else{

									$("#size_cube").attr('disabled', 'disabled');
									$("#operation").attr('disabled', 'disabled');
									$("#btn_op").attr('disabled', 'disabled');
									$("#number_tests").val('');
									$("#number_tests").attr('disabled', 'disabled');
									$("#btn_test").attr('disabled', 'disabled');
								}
							}
							}
						},
						error: function(){

						}
					});
				}
				
			}
		});
	});
</script>
