<?php

/**

1) Se utilizaron nombres de id mas descriptivos, de acuerdo a la entidad, para mejorar la legibilidad del código
2) Si el servicio es nulo se sale enseguida del método, lo que hace que sea mas legible el código
3) Se redujo la cantidad de llamadas a base de datos, a) unificando un update de service b) eliminando una búsqueda innecesaria, lo que contribuye a mejorar la eficiencia del método.
4) se creó un nuevo método que se encarga de enviar el mensaje push, lo que contribuye a separar la funcionalidad de los métodos y reutilizar el código.

**/
public function post_confirm(){
	$service_id = $request->input('service_id');
	$service = Service->find($service_id);

	if($service == NULL){
		return response()->json(array('error' => '3'));
	}
	
	if ($service->status_id == '6') {
		return response()->json(array('error'=>'2');
	}

	if($service->driver_id == NULL && $service->status_id == '1'){

		$driver_id = $request->input('driver_id');

		$driver = Driver::update($driver_id, array('available'=>'0'));

		$service = Service::update($service_id, array(
						'car_id' => $driver->car_id, 
						'driver_id' => $driver->id, 
						'status_id' => '2'));

		if($service->user->uuid != ''){
			$pushMessage = 'Tu servicio ha sido confirmado!';
			send_push($service, $pushMessage);
		}

		return response()->json(array('error' => '0'));
	}
	else{
		return response()->json(array('error' => '1'));
	}
}

public function send_push($service, $message){
	$push = Push::make();
	if($service->user->type == '1'){
		$result = $push->ios($service->user->uuid, $message, 1, 'honk.wak', 'Open', array('serviceId'=>$service->id));
	}
	else{
		$result = $push->android2($service->user->uuid, $message, 1, 'default', 'Open', array('serviceId'=>$service->id));
	}
}

?>
